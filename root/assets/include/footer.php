<footer class="c-footer">
	<div class="c-footer__wrap">
		<div class="c-footer__inner">
			<div class="c-footer__sitemap">
				<div class="c-footer__block">
					<h4 class="c-footer__ttl pc-only">事業紹介</h4>
					<ul class="c-footer__list">
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">自家発電設備エンジニアリング</a>
						</li>
						<li class="c-footer__item1 pc-only">
							<a href="" class="c-footer__text1">− スタートコントローラー</a>
						</li>
						<li class="c-footer__item1 pc-only">
							<a href="" class="c-footer__text1">− 黒煙削減装</a>
						</li>
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">バイオマス発電設備</a>
						</li>
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">太陽光発電設備</a>
						</li>
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">部品資材部</a>
						</li>
					</ul>
				</div>
				<div class="c-footer__block pc-only">
					<h4 class="c-footer__ttl">実績紹介</h4>
					<ul class="c-footer__list">
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">導入ケース</a>
						</li>
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">自家発電設備事業の実績</a>
						</li>
						<li class="c-footer__item1">
							<a href="" class="c-footer__text1">再生可能エネルギー事業の実績</a>
						</li>
					</ul>
				</div>
				<div class="c-footer__block">
					<h4 class="c-footer__ttl pc-only">企業情報</h4>
					<div class="pc-only">
						<ul class="c-footer__list">
							<li class="c-footer__item1">
								<a href="" class="c-footer__text1">企業情報トップ</a>
							</li>
							<li class="c-footer__item1">
								<a href="" class="c-footer__text1">経営者紹介</a>
							</li>
							<li class="c-footer__item1">
								<a href="" class="c-footer__text1">会社概要</a>
							</li>
							<li class="c-footer__item1">
								<a href="" class="c-footer__text1">創業者  波多野龍吉</a>
							</li>
							<li class="c-footer__item1">
								<a href="" class="c-footer__text1">沿革</a>
							</li>
						</ul>
					</div>
					<ul class="c-footer__list2">
						<li class="c-footer__item2 sp-only">
							<a href="" class="c-footer__text2">企業情報</a>
						</li>
						<li class="c-footer__item2">
							<a href="" class="c-footer__text2">SGDsへの取り組み</a>
						</li>
						<li class="c-footer__item2">
							<a href="" class="c-footer__text2">リクルート</a>
						</li>
						<li class="c-footer__item2">
							<a href="" class="c-footer__text2">お知らせ</a>
						</li>
						<li class="c-footer__item2">
							<a href="" class="c-footer__text2">お問い合わせ</a>
						</li>
					</ul>
				</div>
			</div>
			<h1 class="c-footer__logo">
				<a href="/">
					<img src="/assets/img/common/logo-footer.png" alt="" width="56" height="90">
				</a>
			</h1>
			<div class="c-footer__contact">
				<div class="c-footer__time">
					<p class="c-footer__text3">お電話でのお問い合わせ</p>
					<p class="c-footer__text3">［平日受付］ 9:00〜18:00</p>
				</div>
				<a href="tel:0357406261" class="c-footer__phone"><span>TEL </span>03-5740-6261</a>
			</div>
		</div>
	</div>
</footer>

<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/functions.min.js"></script>
</body>
</html>