<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<?php include('meta.php'); ?>
	<link href="/assets/css/style.min.css" rel="stylesheet">
	<link href="/assets/css/slick/slick.css" rel="stylesheet">
	<script src="/assets/js/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="/assets/js/jquery.matchHeight-min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">

	<header class="c-header">
		<div class="c-header__inner">
			<h1 class="c-header__logo">
				<a href="/">
					<img src="/assets/img/common/logo.png" alt="" width="180" height="31">
				</a>
			</h1>

			<div class="c-menuSP">
				<div class="c-menuSP__1"></div>
				<div class="c-menuSP__2"></div>
				<div class="c-menuSP__3"></div>
			</div>

			<div class="c-header__wrap">
				<div class="c-header__info">
					<div class="c-header__contact">
						<p class="c-header__text2">部品資材部</p>
						<a href="tel:0357406267" class="c-header__phone"><span>TEL</span>03-5740-6267</a>
					</div>
					<ul class="c-header__menu">
						<li class="c-header__item">
							<a class="c-header__text1 is-submenu">事業紹介</a>
							<nav class="c-submenu c-submenu--col4">
								<ul class="c-submenu__inner">
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">自家発電設備エンジニアリング</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">バイオマス発電設備</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">太陽光発電設備</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">部品資材部</a>
									</li>
								</ul>
							</nav>
						</li>
						<li class="c-header__item">
							<a class="c-header__text1 is-submenu">実績紹介</a>
							<nav class="c-submenu">
								<ul class="c-submenu__inner">
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">導入ケース</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">自家発電設備事業の実績一覧</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">再生可能エネルギー事業の実績一覧</a>
									</li>
								</ul>
							</nav>
						</li>
						<li class="c-header__item">
							<a class="c-header__text1 is-submenu">企業情報</a>
							<nav class="c-submenu c-submenu--col5">
								<ul class="c-submenu__inner">
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">企業情報トップ</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">経営者紹介</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">会社概要</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">創業者  波多野龍吉</a>
									</li>
									<li class="c-submenu__item">
										<a href="#" class="c-submenu__text">沿革</a>
									</li>
								</ul>
							</nav>
						</li>
						<li class="c-header__item">
							<a href="#" class="c-header__text1">SDGsへの取り組み </a>
						</li>
						<li class="c-header__item">
							<a href="#" class="c-header__text1">リクルート</a>
						</li>
						<li class="c-header__item">
							<a href="#" class="c-header__text1">お間い合わせ</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>