<?php /*========================================
c-breadcrumb
================================================*/ ?>
<nav class="c-breadcrumb">
	<ul class="c-breadcrumb__inner">
		<li class="c-breadcrumb__item">
			<a href="" class="c-breadcrumb__text">事業紹介</a>
		</li>
		<li class="c-breadcrumb__item">
			<span class="c-breadcrumb__text">自家発電設備事業</span>
		</li>
	</ul>
</nav>