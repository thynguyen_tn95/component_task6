<?php /*========================================
form
================================================*/ ?>
<div class="c-dev-title1">form</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-form1</div>
<form action="./" class="c-form1">
	<p class="c-form1__note"><span>*</span>印の項目は必ずご記入お願いいたします。</p>
	<div class="c-form1__block">
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">会社／団体名<span>*</span></p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">フリガナ（会社／団体名）<span>*</span></p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">お名前<span>*</span></p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">フリガナ（お名前）<span>*</span></p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">メールアドレス<span>*</span></p>
				<p class="c-form1__note2">半角英数字記号で入力してください</p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">電話番号</p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">FAX番号</p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">郵便番号</p>
			</dt>
			<dd class="c-form1__info">
				<div class="c-form1__input2">
					<input type="text">
				</div>
				<div class="c-form1__input2">
					<input type="text">
				</div>
			</dd>
		</dl>
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">住所</p>
			</dt>
			<dd class="c-form1__info">
				<input type="text" class="c-form1__input1">
			</dd>
		</dl>
	</div>
	<div class="c-form1__block">
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">資料請求</p>
				<p class="c-form1__note2">資料請求の方は、ご希望の資料にチェックを入れてください。［複数選択可］</p>
			</dt>
			<dd class="c-form1__info">
				<div class="c-form1__list">
					<div class="c-checkbox">
						<input type="checkbox" name="a1" id="a1" checked />
						<label for="a1" class="c-checkbox__text">会社・サービス概要</label>
					</div>
					<div class="c-checkbox">
						<input type="checkbox" name="a2" id="a2" />
						<label for="a2" class="c-checkbox__text">実績集</label>
					</div>
					<div class="c-checkbox">
						<input type="checkbox" name="a3" id="a3" />
						<label for="a3" class="c-checkbox__text">発電事業</label>
					</div>
					<div class="c-checkbox">
						<input type="checkbox" name="a4" id="a4" />
						<label for="a4" class="c-checkbox__text">環境事業</label>
					</div>
				</div>
			</dd>
		</dl>
	</div>
	<div class="c-form1__block">
		<dl class="c-form1__item">
			<dt class="c-form1__ttl">
				<p class="c-form1__text1">お問い合わせ内容<span>*</span></p>
			</dt>
			<dd class="c-form1__info">
				<textarea name="info" rows="7"></textarea>
			</dd>
		</dl>
	</div>
	<div class="c-form1__block">
		<div class="c-form1__privacy">
			<div class="c-form1__privacy__info">
				<p class="c-form1__text1">個人情報の取り扱いについて</p>
				<p class="c-form1__note2">個人情報の取り扱いについてはサイトポリシーをご参照ください。</p>
			</div>
			<div class="c-form1__btn">
				<div class="c-btn3">
					<a href="" class="c-btn3__text">サイトポリシー</a>
				</div>
			</div>
		</div>
	</div>
	<div class="l-btn l-btn--center">
		<div class="c-btn4">
			<a href="" class="c-btn4__text">送信内容の確認</a>
		</div>
	</div>
</form>