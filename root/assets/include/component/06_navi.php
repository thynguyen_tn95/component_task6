<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-navi1</div>
<nav class="c-navi1">
	<ul class="c-navi1__inner">
		<li class="c-navi1__item is-active">
			<a href="#" class="c-navi1__text">バイオマス発電設備</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">太陽光発電設備</a>
		</li>
	</ul>
</nav>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-navi1 c-navi1--col3</div>
<nav class="c-navi1 c-navi1--col3">
	<ul class="c-navi1__inner">
		<li class="c-navi1__item is-active">
			<a href="#" class="c-navi1__text">導入ケース</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">自家発電設備事業の実績一覧</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">再生可能エネルギー事業の実績一覧</a>
		</li>
	</ul>
</nav>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-navi1 c-navi1--col4</div>
<nav class="c-navi1 c-navi1--col4">
	<ul class="c-navi1__inner">
		<li class="c-navi1__item is-active">
			<a href="#" class="c-navi1__text">常用・非常用発電設備</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">保守・メンテナンス</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">スタートコントローラー</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">黒煙削減装置</a>
		</li>
	</ul>
</nav>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-navi1 c-navi1--col5</div>
<nav class="c-navi1 c-navi1--col5">
	<ul class="c-navi1__inner">
		<li class="c-navi1__item is-active">
			<a href="#" class="c-navi1__text">企業情報トップ</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">代表メッセージ</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">会社概要</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">創業者  波多野龍吉</a>
		</li>
		<li class="c-navi1__item">
			<a href="#" class="c-navi1__text">沿革</a>
		</li>
	</ul>
</nav>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-navi2</div>
<nav class="c-navi2">
	<ul class="c-navi2__inner">
		<li class="c-navi2__item is-active">
			<a href="#" class="c-navi2__text">CASE 1</a>
		</li>
		<li class="c-navi2__item">
			<a href="#" class="c-navi2__text">CASE 2</a>
		</li>
		<li class="c-navi2__item">
			<a href="#" class="c-navi2__text">CASE 3</a>
		</li>
		<li class="c-navi2__item">
			<a href="#" class="c-navi2__text">CASE 4</a>
		</li>
		<li class="c-navi2__item">
			<a href="#" class="c-navi2__text">CASE 5</a>
		</li>
	</ul>
</nav>