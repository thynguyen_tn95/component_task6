<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual</div>
<div class="c-mainvisual">
	<ul class="c-mainvisual__slide">
		<li class="c-mainvisual__item">
			<img src="/assets/img/top/100.jpg" alt="" width="1280" height="600">
		</li>
		<li class="c-mainvisual__item">
			<img src="/assets/img/top/101.jpg" alt="" width="1280" height="600">
		</li>
		<li class="c-mainvisual__item">
			<img src="/assets/img/top/102.jpg" alt="" width="1280" height="600">
		</li>
		<li class="c-mainvisual__item">
			<img src="/assets/img/top/103.jpg" alt="" width="1280" height="600">
		</li>
	</ul>
	<div class="c-mainvisual__info">
		<h2 class="c-mainvisual__ttl">願いもしない<br>一瞬のために、<br>生きていく。</h2>
	</div>
	<div class="c-mainvisual__new">
		<div class="c-new">
			<h2 class="c-new__ttl">WHAT'S NEW</h2>
			<ul class="c-new__list">
				<li class="c-new__item">
					<div class="c-new__info">
						<time datetime="2019-03-01" class="c-new__time">2019年3月1日</time>
						<a href="#" class="c-new__name">2020年新卒採用募集開始しました</a>
					</div>
				</li>
				<li class="c-new__item">
					<div class="c-new__info">
						<time datetime="2019-03-02" class="c-new__time">2019年3月2日</time>
						<a href="#" class="c-new__name">Dummy Dummy</a>
					</div>
				</li>
				<li class="c-new__item">
					<div class="c-new__info">
						<time datetime="2019-03-03" class="c-new__time">2019年3月3日</time>
						<a href="#" class="c-new__name">Lorem ipsum dolor.Lorem ipsum dolor.Lorem ipsum dolor.Lorem ipsum dolor.Lorem ipsum dolor.</a>
					</div>
				</li>
			</ul>
			<div class="c-new__more">
				<a href="#">お知らせ一覧</a>
			</div>
		</div>
	</div>
</div>

<br>
<br>
<br>
<br>
<br>
<br>

<div class="c-mainvisual">
	<div class="c-mainvisual__img">
		<img src="/assets/img/business/introduction/100.jpg" alt="" width="1280" height="650">
	</div>
</div>

<br>
<br>
<br>
<br>
<br>
<br>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual2</div>
<div class="c-mainvisual2">
	<div class="c-mainvisual2__img">
		<img src="/assets/img/company/introduction/100.jpg" alt="" width="1280" height="650">
	</div>
	<div class="c-mainvisual2__inner">
		<div class="c-mainvisual2__info">
			<p class="c-mainvisual2__text">株式会社ハタノシステム<br>代表取締役社長　波多野 裕一</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual3</div>
<div class="c-mainvisual3">
	<div class="c-mainvisual3__img">
		<img src="/assets/img/company/entrepreneur/100.jpg" alt="" width="1280" height="550">
	</div>
	<div class="c-mainvisual3__info">
		<p class="c-mainvisual3__text">創業者 　 波多野龍吉</p>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual4</div>
<div class="c-mainvisual4">
	<div class="c-mainvisual4__img">
		<img src="/assets/img/news/100.jpg" alt="" width="1280" height="100">
	</div>
	<div class="c-mainvisual4__info">
		<p class="c-mainvisual4__text">WHAT'S NEW</p>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-new</div>
<!-- <div class="c-new">
	<h2 class="c-new__ttl">WHAT'S NEW</h2>
	<ul class="c-new__list">
		<li class="c-new__item">
			<div class="c-new__info">
				<time datetime="2019-03-01" class="c-new__time">2019年3月1日</time>
				<a href="#" class="c-new__name">2020年新卒採用募集開始しました</a>
			</div>
		</li>
		<li class="c-new__item">
			<div class="c-new__info">
				<time datetime="2019-03-02" class="c-new__time">2019年3月2日</time>
				<a href="#" class="c-new__name">Dummy Dummy</a>
			</div>
		</li>
		<li class="c-new__item">
			<div class="c-new__info">
				<time datetime="2019-03-03" class="c-new__time">2019年3月3日</time>
				<a href="#" class="c-new__name">Lorem ipsum dolor.</a>
			</div>
		</li>
	</ul>
	<div class="c-new__more">
		<a href="#">お知らせ一覧</a>
	</div>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-flow1</div>
<!-- <div class="c-flow1">
	<div class="c-flow1__inner c-flow1--col1">
		<div class="c-flow1__item">
			<h3 class="c-flow1__ttl">企画・設計</h3>
			<p class="c-flow1__text">施設に合った自家発電設備を1から設計</p>
		</div>
	</div>
	<div class="c-flow1__inner c-flow1--col2">
		<div class="c-flow1__item">
			<h3 class="c-flow1__ttl">保守・メンテナンス</h3>
			<p class="c-flow1__text">メンテナンスのスペシャリストが<br>定期的にメンテナンス</p>
		</div>
		<div class="c-flow1__item">
			<h3 class="c-flow1__ttl">施工管理</h3>
			<p class="c-flow1__text">オリジナル設備やメーカー品などを<br>安全な施工管理で導入</p>
		</div>
	</div>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-flow2</div>
<!-- <ul class="c-flow2">
	<li class="c-flow2__item">
		<h3 class="c-flow2__ttl1">企画・設計</h3>
		<div class="c-flow2__detail">
			<div class="c-flow2__info">
				<p class="c-flow2__ttl2">独自の技術で最適なプランニングをいたします</p>
				<p class="c-flow2__text">業務の内容と状況、施設のタイプ、建物や空間の規模などお客様のご事情により、自家発電設備のスペックも異なります。60年以上の歴史の中で、官民ともに豊富な実績を残してきた当社ならではの独自技術を活かしてフレキシブルかつスピーディーに最適なプランニングをご提供します。</p>
			</div>
			<div class="c-flow2__img">
				<img src="/assets/img/business/facility/100.jpg" alt="" width="274" height="180">
			</div>
		</div>
	</li>
	<li class="c-flow2__item">
		<h3 class="c-flow2__ttl1">施工管理</h3>
		<div class="c-flow2__detail">
			<div class="c-flow2__info">
				<p class="c-flow2__ttl2">特殊な現場も安全にしっかり納期管理いたします</p>
				<p class="c-flow2__text">創立から長い歳月をかけて、技術力の向上に努めてきたハタノシステムでは、多くの社員が第3種電気主任技術者、1級電気工事施工管理技術士、第１種電気工事士、特殊電気工事資格者(発電設備)ほかの資格を保有する、技術のスペシャリスト集団です。また、品質管理システムに関する国際規格「ISO9001」の認証も取得。お客様からお寄せいただく信頼にお応えする、高品質な施工を実践しています。</p>
			</div>
			<div class="c-flow2__img">
				<img src="/assets/img/business/facility/101.jpg" alt="" width="274" height="180">
			</div>
		</div>
	</li>
	<li class="c-flow2__item">
		<h3 class="c-flow2__ttl1">保守・メンテナンス</h3>
		<div class="c-flow2__detail">
			<ul class="c-list3 c-list3--col1">
				<li class="c-list3__item c-list3__item--gray">
					<p class="c-list3__text">メンテナンスのスペシャリストが定期的にメンテナンスいたします</p>
				</li>
			</ul>
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-flow3</div>
<!-- <ul class="c-flow3">
	<li class="c-flow3__item">
		<h3 class="c-flow3__ttl">スタートコントローラー不使用時</h3>
		<p class="c-flow3__text">発電機 199.3kVA<br>エンジン 224.8kW</p>
	</li>
	<li class="c-flow3__item">
		<h3 class="c-flow3__ttl">スタートコントローラー使用時</h3>
		<p class="c-flow3__text">発電機142.3kVA<br>エンジン 130.5kW</p>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-flow4</div>
<!-- <ul class="c-flow4">
	<li class="c-flow4__item">
		<p class="c-flow4__text">間伐材</p>
	</li>
	<li class="c-flow4__item">
		<p class="c-flow4__text">チップ</p>
	</li>
	<li class="c-flow4__item c-flow4__item--icon">
		<p class="c-flow4__text">バイオマスガス化コージェネレーション</p>
	</li>
	<li class="c-flow4__item">
		<p class="c-flow4__text">公営温浴施設・宿舎・プール</p>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-info1</div>
<!-- <div class="c-info1">
	<div class="c-title5">
		<h3 class="c-title5__text">メンテナンス万全、24時間365日の監視体制</h3>
	</div>
	<p class="c-info1__text">ハタノシステムがご提供するコージェネレーションシステムは、すべて純国産（ヤンマー製）ですので、運用後のメンテナンスも安心。24時間365日体制で稼動を監視し、万一のトラブル発生時は迅速に対応します。</p>
</div>
<div class="c-info1">
	<div class="c-title5">
		<h3 class="c-title5__text">初期コストに対する補助金、電気の買取制度も</h3>
	</div>
	<p class="c-info1__text">システムの導入にあたっては、環境省の「地域グリーンニューディール基金」や農林水産省の「地域バイオマス利活用交付金」などにより、初期コストの負担を大幅に軽減できます。さらに運用後は「グリーン電力証書」の販売に加え、売電を可能にする資源エネルギー庁の「再生可能エネルギーの全量買取制度」も施行が予定されており、収益も見込まれます。</p>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-case1</div>
<!-- <div class="c-case1">
	<a href="#" class="c-case1__inner">
		<div class="c-case1__name">
			<p class="c-case1__text1">CASE <span>1</span></p>
		</div>
		<div class="c-case1__info">
			<h3 class="c-case1__ttl">杏林大学 医学部附属病院 様</h3>
			<p class="c-case1__text2">新規導入時のエンジニアリングからメンテナンスまで、一貫体制のトータルソリューションを提供します。</p>
		</div>
		<div class="c-case1__img">
			<img src="/assets/img/results/introduction/100.jpg" alt="" width="200" height="200">
		</div>
	</a>
</div>
<div class="c-case1">
	<a href="#" class="c-case1__inner">
		<div class="c-case1__name">
			<p class="c-case1__text1">CASE <span>2</span></p>
		</div>
		<div class="c-case1__info">
			<h3 class="c-case1__ttl">生長の家 森の中のオフィス 様</h3>
			<p class="c-case1__text2">発電設備メーカーの最先端技術を吸収し、新しいエネルギー作りにも積極的に取り組んでいます。</p>
		</div>
		<div class="c-case1__img">
			<img src="/assets/img/results/introduction/101.jpg" alt="" width="200" height="200">
		</div>
	</a>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-intro1</div>
<!-- <div class="c-intro1">
	<p class="c-intro1__ttl">導入実績</p>
	<ul class="c-intro1__list">
		<li class="c-intro1__item">
			<p class="c-intro1__text">1500kVA×2台</p>
		</li>
		<li class="c-intro1__item">
			<p class="c-intro1__text">1250kVA×3台</p>
		</li>
		<li class="c-intro1__item">
			<p class="c-intro1__text">300kVA×1台</p>
		</li>
		<li class="c-intro1__item">
			<p class="c-intro1__text">80kVA×1台</p>
		</li>
	</ul>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-intro2</div>
<!-- <div class="c-intro2">
	<a href="#" class="c-intro2__inner">
		<div class="c-intro2__img">
			<img src="/assets/img/SDGs/107.jpg" alt="" width="600" height="321">
		</div>
		<div class="c-intro2__detail">
			<div class="c-title9 c-title9--icon">
				<h3 class="c-title9__text">ディーゼル発電設備</h3>
			</div>
			<div class="c-intro2__info">
				<p class="c-intro2__text">停電時に電源供給を止めない非常用自家発電設備の普及を促進します</p>
			</div>
			<ul class="c-list13">
				<li class="c-list13__item">
					<div class="c-list13__img">
						<img src="/assets/img/SDGs/101.jpg" alt="" width="119" height="120">
					</div>
				</li>
				<li class="c-list13__item">
					<div class="c-list13__img">
						<img src="/assets/img/SDGs/102.jpg" alt="" width="119" height="120">
					</div>
				</li>
			</ul>
		</div>
	</a>
</div>
<div class="c-intro2">
	<a href="#" class="c-intro2__inner">
		<div class="c-intro2__img">
			<img src="/assets/img/SDGs/108.jpg" alt="" width="600" height="321">
		</div>
		<div class="c-intro2__detail">
			<div class="c-title9 c-title9--icon">
				<h3 class="c-title9__text">バイオマス発電設備・太陽光発電設備</h3>
			</div>
			<div class="c-intro2__info">
				<p class="c-intro2__text">間伐材を有効活用したバイオマス発電設備と太陽の光から電気を作り出す太陽光発電設備でクリーンエネルギーの普及を促進します</p>
			</div>
			<ul class="c-list13">
				<li class="c-list13__item">
					<div class="c-list13__img">
						<img src="/assets/img/SDGs/103.jpg" alt="" width="119" height="120">
					</div>
				</li>
				<li class="c-list13__item">
					<div class="c-list13__img">
						<img src="/assets/img/SDGs/101.jpg" alt="" width="119" height="120">
					</div>
				</li>
			</ul>
		</div>
	</a>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link1</div>
<div class="l-container">
	<div class="c-link1">
		<a href="#" class="c-link1__text">マイナビ2019へ</a>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link2</div>
<div class="l-container">
	<div class="c-link2">
		<a href="#" class="c-link2__text">お知らせ一覧に戻る</a>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-archive</div>
<div class="l-container">
	<div class="c-archive">
		<h3 class="c-archive__ttl">アーカイブ</h3>
		<ul class="c-archive__list">
			<li class="c-archive__item">
				<a href="#" class="c-archive__text">2020年（1）</a>
			</li>
			<li class="c-archive__item">
				<a href="#" class="c-archive__text">2019年（5）</a>
			</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-archive</div>
<div class="l-container">
	<div class="c-checkbox">
		<input type="checkbox" name="a" id="a" checked />
		<label for="a" class="c-checkbox__text">会社・サービス概要</label>
	</div>
</div>