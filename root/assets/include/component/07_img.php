<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgSingle</div>
<div class="c-imgSingle">
	<img src="/assets/img/business/sutakon/100.jpg" alt="" width="700" height="351">
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext1</div>
<div class="c-imgtext1">
	<div class="c-imgtext1__img">
		<img src="/assets/img/business/facility/105.jpg" alt="" width="370" height="201">
	</div>
	<div class="c-imgtext1__info">
		<h3 class="c-imgtext1__ttl">災害時・停電時などの非常用電源として、自治体や電力会社などに導入されています</h3>
		<p class="c-imgtext1__text">トラックとエンジン発電機を一体化した移動電源車は、停電時にライフラインである電源の確保や、高度情報化社会の基幹システムであるオンラインシステム・情報通信システムへの電力供給に威力を発揮します。また、野外コンサートなどイベントの電源確保にも最適です。1台で複数負荷に対応でき、負荷容量が大きい場合は並列運転も可能。容量・電圧・操作方法などお客様のニーズに合わせたシステム設計も行えます。</p>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext2</div>
<!-- <div class="c-imgtext2">
	<div class="c-imgtext2__img">
		<img src="/assets/img/company/profile/100.jpg" alt="" width="117" height="117">
	</div>
	<div class="c-imgtext2__info">
		<div class="c-imgtext2__block">
			<h3 class="c-imgtext2__ttl">Vision</h3>
			<p class="c-imgtext2__text">自分以外の誰かを大切にする世の中をつくりたい。</p>
		</div>
		<div class="c-imgtext2__block">
			<h3 class="c-imgtext2__ttl">Mission</h3>
			<p class="c-imgtext2__text">突き抜けた知識、技術、チームワークで<br>発電システムのナンバーワンになる。</p>
		</div>
	</div>
</div>
-->
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext3</div>
<!-- <div class="c-imgtext3">
	<div class="c-imgtext3__img">
		<img src="/assets/img/SDGs/100.png" alt="" width="400" height="90">
	</div>
	<div class="c-imgtext3__info">
		<h3 class="c-imgtext3__ttl">世界を変えるための17の目標</h3>
		<p class="c-imgtext3__text">SDGs（持続可能な開発目標：Sustainable Development Goals）は、2015年に国連総会で採択され、2030年までの15年間で達成する国際目標となっています。貧困や格差、気候変動など、17の目標と169のターゲットを掲げています。</p>
		<p class="c-imgtext3__text">私たちハタノシステムは、非常用自家発電設備、太陽光事業、バイオマス事業、ミャンマーでの植物栽培、中古自家発電の再利用、女性管理職の登用など、17の目標に掲げられている「ジェンダー平等の実現」「クリーンなエネルギー開発」「産業と技術革新の基盤づくり」などの項目で業界を先駆けて取り組んでいます。</p>
	</div>
</div> -->