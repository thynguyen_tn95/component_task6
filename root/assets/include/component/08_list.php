<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<ul class="c-list1">
	<li class="c-list1__item">
		<a href="#" class="c-list1__inner">
			<p class="c-list1__text">自家発電設備エンジニアリング</p>
		</a>
	</li>
	<li class="c-list1__item">
		<a href="#" class="c-list1__inner">
			<p class="c-list1__text">バイオマス発電設備</p>
		</a>
	</li>
	<li class="c-list1__item">
		<a href="#" class="c-list1__inner">
			<p class="c-list1__text">太陽光発電設備</p>
		</a>
	</li>
</ul>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<ul class="c-list2">
	<li class="c-list2__item">
		<h3 class="c-list2__ttl1">Message</h3>
		<a href="#" class="c-list2__inner">
			<div class="c-list2__img">
				<img src="/assets/img/top/104.jpg" alt="" width="620" height="300" class="pc-only">
				<img src="/assets/img/top/104_sp.jpg" alt="" width="700" height="301" class="sp-only">
			</div>
			<div class="c-list2__info1">
				<p class="c-list2__text1">経営者紹介</p>
			</div>
		</a>
	</li>
	<li class="c-list2__item">
		<h3 class="c-list2__ttl1">Recruitment Information</h3>
		<a href="#" class="c-list2__inner">
			<div class="c-list2__img">
				<img src="/assets/img/top/105.jpg" alt="" width="620" height="300" class="pc-only">
				<img src="/assets/img/top/105_sp.jpg" alt="" width="700" height="301" class="sp-only">
			</div>
			<div class="c-list2__info2">
				<h4 class="c-list2__ttl2">RECRUIT</h4>
				<p class="c-list2__text2">私の仕事は、この街を再稼働させること。</p>
			</div>
		</a>
	</li>
	<li class="c-list2__item">
		<h3 class="c-list2__ttl1">Our DNA</h3>
		<a href="#" class="c-list2__inner">
			<div class="c-list2__img">
				<img src="/assets/img/top/106.jpg" alt="" width="620" height="300" class="pc-only">
				<img src="/assets/img/top/106_sp.jpg" alt="" width="700" height="301" class="sp-only">
			</div>
		</a>
	</li>
	<li class="c-list2__item">
		<h3 class="c-list2__ttl1">SDGs</h3>
		<a href="#" class="c-list2__inner">
			<div class="c-list2__img">
				<img src="/assets/img/top/107.jpg" alt="" width="620" height="300" class="pc-only">
				<img src="/assets/img/top/107_sp.jpg" alt="" width="700" height="301" class="sp-only">
			</div>
			<div class="c-list2__info1">
				<p class="c-list2__text1">SDGsへの取り組み</p>
			</div>
		</a>
	</li>
</ul>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<!-- <ul class="c-list3">
	<li class="c-list3__item c-list3__item--height">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">常用・非常用発電設備</p>
		</a>
	</li>
	<li class="c-list3__item c-list3__item--height">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">保守・メンテナンス</p>
		</a>
	</li>
	<li class="c-list3__item">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">スタートコントローラー</p>
		</a>
	</li>
	<li class="c-list3__item">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">黒煙削減装置</p>
		</a>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3 c-list3--col1</div>
<!-- <ul class="c-list3 c-list3--col1">
	<li class="c-list3__item">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">部品レスキュー</p>
		</a>
	</li>
</ul>
-->
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3 c-list3--col1 > c-list3__item--gray</div>
<!-- <ul class="c-list3 c-list3--col1">
	<li class="c-list3__item c-list3__item--gray">
		<a href="#" class="c-list3__inner">
			<p class="c-list3__text">実績紹介</p>
		</a>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list4</div>
<!-- <ul class="c-list4">
	<li class="c-list4__item">
		<h3 class="c-list4__ttl">ディーゼル発電設備［キュービクル式］</h3>
		<div class="c-list4__img">
			<img src="/assets/img/business/facility/102.jpg" alt="" width="380" height="201">
		</div>
		<div class="c-list4__info">
			<p class="c-list4__text">いざという時に信頼性の高いコンパクトタイプ。</p>
		</div>
	</li>
	<li class="c-list4__item">
		<h3 class="c-list4__ttl">ディーゼル発電設備［オープン式］</h3>
		<div class="c-list4__img">
			<img src="/assets/img/business/facility/103.jpg" alt="" width="380" height="201">
		</div>
		<div class="c-list4__info">
			<p class="c-list4__text">幅広い用途に対応する放水式。</p>
		</div>
	</li>
	<li class="c-list4__item">
		<h3 class="c-list4__ttl">ガスタービン発電設備</h3>
		<div class="c-list4__img">
			<img src="/assets/img/business/facility/104.jpg" alt="" width="380" height="201">
		</div>
		<div class="c-list4__info">
			<p class="c-list4__text">軽量・コンパクトで振動が小さく、水が不要。</p>
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list5</div>
<!-- <ul class="c-list5">
	<li class="c-list5__item">
		<h3 class="c-list5__ttl">定期点検</h3>
		<p class="c-list5__text">半年・一年点検でベストの状態にします</p>
	</li>
	<li class="c-list5__item">
		<h3 class="c-list5__ttl">オーバーホール</h3>
		<p class="c-list5__text">発電機をすみずみまで点検し、新品同様にリフレッシュします</p>
	</li>
	<li class="c-list5__item">
		<h3 class="c-list5__ttl">緊急・非常時対応</h3>
		<p class="c-list5__text">優秀なサービスマンが24時間体制でお客様のもとへ</p>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list6</div>
<!-- <ul class="c-list6">
	<li class="c-list6__item">
		<p class="c-list6__text">通常の消防点検だけで大丈夫だろうか</p>
	</li>
	<li class="c-list6__item">
		<p class="c-list6__text">古くなった発電装置を、オーバーホールかリニューアルかで迷っている</p>
	</li>
	<li class="c-list6__item">
		<p class="c-list6__text">発電装置の状態を診断したい</p>
	</li>
	<li class="c-list6__item">
		<p class="c-list6__text">発電装置の長期点検整備計画を立てたい</p>
	</li>
	<li class="c-list6__item">
		<p class="c-list6__text">発電装置の技術研修を行いたい</p>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list7</div>
<!-- <ul class="c-list7">
	<li class="c-list7__item">
		<h3 class="c-list7__ttl">自家発電の設備コスト、運用コストを低減します</h3>
		<p class="c-list7__text">複数の機器を順次始動させることで、自家発電設備の容量を最小限に抑えるハタノシステム独自開発のスタートコントローラーです。容量低減により、設備コスト（設置スペース等）はもちろん運用コストも低減。自家発電設備の効率的かつ合理的な運用をサポートします。</p>
	</li>
	<li class="c-list7__item">
		<h3 class="c-list7__ttl">1台で最大5台までの防災・消防設備に対応できます</h3>
		<p class="c-list7__text">消防法にて定められている「複数台の防災･消防設備は同時始動」及び「なるべく早く始動（40秒以内）する」ことが望ましいものの、複数台の機器を同時に始動させると始動時に多大なエネルギーが必要となり、自家発電設備の容量が大きくなります。スタートコントローラーを活用すれば、1台で最大5台までの機器を希望の順序で自動的（5秒以内）に順次始動可能。複数台を組み合わせることで、機器が6台以上の場合にも対応できます。</p>
	</li>
	<li class="c-list7__item">
		<h3 class="c-list7__ttl">設定した順番で複数の機器を自動的に始動します</h3>
		<p class="c-list7__text">下記1〜5の機器を、順番を決めて始動することが可能です。No.1の消火栓ポンプが始動しない場合には、5秒を待たずにNo.2の泡消火ポンプを始動。次にNo.3の排煙ファンとNo.1の始動が重なった場合は、No.1を優先します。その次に、No.3の始動という流れになります。</p>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list8</div>
<!-- <ul class="c-list8">
	<li class="c-list8__item">
		<div class="c-title3 c-title3--yellow">
			<h2 class="c-title3__text">メリット 1</h2>
		</div>
		<div class="c-list8__info js_matchHeight">
			<p class="c-list8__text">屋上や屋根など<br>有休空間の利用</p>
		</div>
	</li>
	<li class="c-list8__item">
		<div class="c-title3 c-title3--yellow">
			<h2 class="c-title3__text">メリット 2</h2>
		</div>
		<div class="c-list8__info js_matchHeight">
			<p class="c-list8__text">CO2排出量の削減</p>
		</div>
	</li>
	<li class="c-list8__item">
		<div class="c-title3 c-title3--yellow">
			<h2 class="c-title3__text">メリット 3</h2>
		</div>
		<div class="c-list8__info js_matchHeight">
			<p class="c-list8__text">広告宣伝効果</p>
		</div>
	</li>
	<li class="c-list8__item">
		<div class="c-title3 c-title3--yellow">
			<h2 class="c-title3__text">メリット 4</h2>
		</div>
		<div class="c-list8__info js_matchHeight">
			<p class="c-list8__text">遮蔽効果による省エネ</p>
		</div>
	</li>
	<li class="c-list8__item">
		<div class="c-title3 c-title3--yellow">
			<h2 class="c-title3__text">メリット 5</h2>
		</div>
		<div class="c-list8__info js_matchHeight">
			<p class="c-list8__text">電気料金の軽減</p>
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list9</div>
<!-- <div class="l-container">
	<div class="c-list9">
		<h3 class="c-list9__ttl">取り扱い品目</h3>
		<ul class="c-list9__info">
			<li class="c-list9__item">
				<p class="c-list9__text">発電機エンジン部品</p>
			</li>
			<li class="c-list9__item">
				<p class="c-list9__text">建設機械部品</p>
			</li>
			<li class="c-list9__item">
				<p class="c-list9__text">各種電動ポンプ</p>
			</li>
			<li class="c-list9__item">
				<p class="c-list9__text">バッテリー</p>
			</li>
			<li class="c-list9__item">
				<p class="c-list9__text">潤滑油</p>
			</li>
			<li class="c-list9__item">
				<p class="c-list9__text">クーラント　など</p>
			</li>
		</ul>
	</div>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list10</div>
<!-- <ul class="c-list10">
	<li class="c-list10__card">
		<div class="c-list9">
			<h3 class="c-list9__ttl">取り扱い品目</h3>
			<ul class="c-list9__info">
				<li class="c-list9__item">
					<p class="c-list9__text">発電機エンジン部品</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">建設機械部品</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">各種電動ポンプ</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">バッテリー</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">潤滑油</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">クーラント　など</p>
				</li>
			</ul>
		</div>
	</li>
	<li class="c-list10__card">
		<div class="c-list9">
			<h3 class="c-list9__ttl">主な仕入先</h3>
			<ul class="c-list9__info">
				<li class="c-list9__item">
					<p class="c-list9__text">ヤンマー株式会社</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">ヤンマー建機株式会社</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">ヤンマー産業株式会社</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">株式会社荏原製作所</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">株式会社GSユアサ</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">古河電池株式会社</p>
				</li>
				<li class="c-list9__item">
					<p class="c-list9__text">各社エンジンメーカー</p>
				</li>
			</ul>
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list11</div>
<!-- <ul class="c-list11">
	<li class="c-list11__item">
		<div class="c-list11__img">
			<img src="/assets/img/results/case1/100.jpg" alt="" width="290" height="251">
		</div>
		<div class="c-list11__info">
			<p class="c-list11__text">杏林大学病院外観</p>
		</div>
	</li>
	<li class="c-list11__item">
		<div class="c-list11__img">
			<img src="/assets/img/results/case1/101.jpg" alt="" width="290" height="251">
		</div>
		<div class="c-list11__info">
			<p class="c-list11__text">冷却水システム</p>
		</div>
	</li>
	<li class="c-list11__item">
		<div class="c-list11__img">
			<img src="/assets/img/results/case1/102.jpg" alt="" width="290" height="251">
		</div>
		<div class="c-list11__info">
			<p class="c-list11__text">燃料システム</p>
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list12</div>
<!-- <ul class="c-list12">
	<li class="c-list12__item">
		<p class="c-list12__text">1級電気工事施工管理技術士</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">第１種電気工事士</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">特殊電気工事資格者（発電設備）</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">第3種電気主任技術者</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">自家発電設備専門技術者</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">蓄電池設備整備資格者</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">ポンプ施設管理技術者</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">消防点検資格者</p>
	</li>
	<li class="c-list12__item">
		<p class="c-list12__text">危険物取扱者(乙種4類他)</p>
	</li>
</ul>
-->
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list13</div>
<!-- <ul class="c-list13">
	<li class="c-list13__item">
		<div class="c-list13__img">
			<img src="/assets/img/SDGs/101.jpg" alt="" width="119" height="120">
		</div>
	</li>
	<li class="c-list13__item">
		<div class="c-list13__img">
			<img src="/assets/img/SDGs/102.jpg" alt="" width="119" height="120">
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list14</div>
<!-- <ul class="c-list14">
	<li class="c-list14__item">
		<div class="c-list14__img">
			<img src="/assets/img/SDGs/103.jpg" alt="" width="119" height="120">
		</div>
	</li>
	<li class="c-list14__item">
		<div class="c-list14__img">
			<img src="/assets/img/SDGs/105.jpg" alt="" width="100" height="100">
		</div>
	</li>
	<li class="c-list14__item">
		<div class="c-list14__img">
			<img src="/assets/img/SDGs/106.jpg" alt="" width="100" height="100">
		</div>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list15</div>
<!-- <ul class="c-list15">
	<li class="c-list15__item">
		<div class="c-title9">
			<h3 class="c-title9__text">ミャンマーでの植物栽培</h3>
		</div>
		<p class="c-list15__text">バイオマス発電の普及促進とエキスパートの育成を目指します</p>
		<ul class="c-list14">
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/103.jpg" alt="" width="119" height="120">
				</div>
			</li>
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/105.jpg" alt="" width="100" height="100">
				</div>
			</li>
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/106.jpg" alt="" width="100" height="100">
				</div>
			</li>
		</ul>
	</li>
	<li class="c-list15__item">
		<div class="c-title9">
			<h3 class="c-title9__text">中古自家発電設備の再利用促進</h3>
		</div>
		<p class="c-list15__text">省資源化、ゴミの削減、停電の削減とエンジニアの育成を目指します</p>
		<ul class="c-list14">
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/105.jpg" alt="" width="100" height="100">
				</div>
			</li>
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/101.jpg" alt="" width="119" height="120">
				</div>
			</li>
			<li class="c-list14__item">
				<div class="c-list14__img">
					<img src="/assets/img/SDGs/102.jpg" alt="" width="119" height="120">
				</div>
			</li>
		</ul>
	</li>
</ul> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list16</div>
<!-- <div class="c-list16">
	<h3 class="c-list16__ttl">2019年</h3>
	<ul class="c-list16__inner">
		<li class="c-list16__row">
			<div class="c-list16__col c-list16__col1">
				<p class="c-list16__label">2019年3月1日</p>
			</div>
			<div class="c-list16__col c-list16__col2">
				<a href="#" class="c-list16__text">2020年新卒採用募集開始しました</a>
			</div>
		</li>
		<li class="c-list16__row">
			<div class="c-list16__col c-list16__col1">
				<p class="c-list16__label">2019年0月0日</p>
			</div>
			<div class="c-list16__col c-list16__col2">
				<a href="#" class="c-list16__text">国土交通省関東地方整備局下館河川事務所様より表彰されました</a>
			</div>
		</li>
		<li class="c-list16__row">
			<div class="c-list16__col c-list16__col1">
				<p class="c-list16__label">2019年0月0日</p>
			</div>
			<div class="c-list16__col c-list16__col2">
				<a href="#" class="c-list16__text">国土交通省関東地方整備局下館河川事務所様より表彰されました</a>
			</div>
		</li>
		<li class="c-list16__row">
			<div class="c-list16__col c-list16__col1">
				<p class="c-list16__label">2019年0月0日</p>
			</div>
			<div class="c-list16__col c-list16__col2">
				<a href="#" class="c-list16__text">国土交通省関東地方整備局下館河川事務所様より表彰されました</a>
			</div>
		</li>
		<li class="c-list16__row">
			<div class="c-list16__col c-list16__col1">
				<p class="c-list16__label">2018年0月0日</p>
			</div>
			<div class="c-list16__col c-list16__col2">
				<a href="#" class="c-list16__text">国土交通省関東地方整備局下館河川事務所様より表彰されました</a>
			</div>
		</li>
	</ul>
</div> -->