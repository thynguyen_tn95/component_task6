<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title1</div>
<div class="c-title1">
	<h2 class="c-title1__text">自家発電設備事業</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title2</div>
<div class="c-title2">
	<h3 class="c-title2__text">60年以上の実績を誇る常用・非常用発電設備</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title3</div>
<div class="c-title3">
	<h2 class="c-title3__text">場所や用途で選べる自家発電設備</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title3 c-title3--yellow</div>
<div class="c-title3 c-title3--yellow">
	<h2 class="c-title3__text">メリット 1</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title4</div>
<div class="c-title4">
	<h3 class="c-title4__text">効果的な運用をサポート</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title5</div>
<div class="c-title5">
	<h3 class="c-title5__text">間伐材の有効活用で電気・温水を安定供給</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title6</div>
<div class="c-title6">
	<h2 class="c-title6__text">杏林大学 医学部附属病院 様</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title7</div>
<div class="c-title7">
	<h2 class="c-title7__text">会社概要</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title8</div>
<div class="c-title8">
	<h3 class="c-title8__text">WEBサイトご利用にあたっての注意事項</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title9</div>
<div class="l-container">
	<div class="c-title9">
		<h3 class="c-title9__text">ディーゼル発電設備</h3>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title9 c-title9--icon</div>
<div class="l-container">
	<div class="c-title9 c-title9--icon">
		<h3 class="c-title9__text">ディーゼル発電設備</h3>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">.c-title10</div>
<div class="c-title10">
	<div class="c-title10__inner">
		<p class="c-title10__text1">2019年3月1日</p>
		<h3 class="c-title10__text2">2020年新卒採用募集開始しました</h3>
	</div>
</div>