<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table1</div>
<div class="c-table1">
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">No.</dt>
		<dd class="c-table1__col c-table1__col2">機器</dd>
		<dd class="c-table1__col c-table1__col3">容量</dd>
		<dd class="c-table1__col c-table1__col4">始動方式</dd>
	</dl>
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">1</dt>
		<dd class="c-table1__col c-table1__col2">消火栓ポンプ</dd>
		<dd class="c-table1__col c-table1__col3">30kW</dd>
		<dd class="c-table1__col c-table1__col4">スターデルタ</dd>
	</dl>
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">2</dt>
		<dd class="c-table1__col c-table1__col2">泡消化ポンプ</dd>
		<dd class="c-table1__col c-table1__col3">7.5kW</dd>
		<dd class="c-table1__col c-table1__col4">直入れ</dd>
	</dl>
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">3</dt>
		<dd class="c-table1__col c-table1__col2">排煙ファン</dd>
		<dd class="c-table1__col c-table1__col3">5.5kW</dd>
		<dd class="c-table1__col c-table1__col4">直入れ</dd>
	</dl>
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">4</dt>
		<dd class="c-table1__col c-table1__col2">給水ポンプ</dd>
		<dd class="c-table1__col c-table1__col3">10kW</dd>
		<dd class="c-table1__col c-table1__col4">直入れ</dd>
	</dl>
	<dl class="c-table1__row">
		<dt class="c-table1__col c-table1__col1">5</dt>
		<dd class="c-table1__col c-table1__col2">スコットトランス</dd>
		<dd class="c-table1__col c-table1__col3">30kW</dd>
		<dd class="c-table1__col c-table1__col4">単相負荷一般</dd>
	</dl>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table2</div>
<div class="c-table2">
	<h3 class="c-table2__ttl">品川フロントビル 様</h3>
	<div class="c-table2__inner">
		<dl class="c-table2__row">
			<dt class="c-table2__col c-table2__col1">
				<p class="c-table2__label">実績</p>
			</dt>
			<dd class="c-table2__col c-table2__col2">
				<p class="c-table2__text">ガスタービン　2500kVA</p>
			</dd>
		</dl>
		<dl class="c-table2__row">
			<dt class="c-table2__col c-table2__col1">
				<p class="c-table2__label">期間</p>
			</dt>
			<dd class="c-table2__col c-table2__col2">
				<p class="c-table2__text">2011年納入</p>
			</dd>
		</dl>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table3</div>
<div class="c-table3">
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">商号</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">株式会社ハタノシステム</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">英文社名</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">HATANO SYSTEMS CO., LTD.</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">設立</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">1950年6月1日（創立／1946年10月1日）</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">資本金</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">払込資本 6,000万円（授権資本 2億4,000万円）</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">年商</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">37億5,425万円（2018年5月期）</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">代表取締役</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">代表取締役社長　波多野 裕一</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">社員数</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">109名（2018年5月現在）</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">本社</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">〒153-0064　東京都目黒区下目黒2丁目23-18 目黒山手通ビル7階<br>［代表］TEL.03-5740-6261（代）　　FAX.03-5740-6271</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">埼玉サービス<br>センター</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">〒340-0203　埼玉県久喜市桜田2-133-6<br>TEL.0480-57-4810　　FAX.0480-57-1270</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">主要取引先</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">株式会社荏原製作所／鹿島建設株式会社／株式会社関電工／株式会社きんでん／三機工業株式会社／シンフォニアテクノロジー株式会社／大成建設株式会社／東京電力パワーグリッド株式会社／株式会社東光高岳／東光電気工事株式会社／東芝インフラシステムズ株式会社／中日本ハイウェイ・エンジニアリング東京株式会社／日本電設工業株式会社／株式会社ネクスコ東日本エンジニアリング／株式会社日立製作所／富士電機株式会社／株式会社明電舎／株式会社安川電機　（50音順・敬称略）</p>
		</dd>
	</dl>
	<dl class="c-table3__row">
		<dt class="c-table3__col c-table3__col1">
			<p class="c-table3__ttl">許可業種</p>
		</dt>
		<dd class="c-table3__col c-table3__col2">
			<p class="c-table3__text">東京都知事 許可（特定）　第15673号 電気工事<br>東京都知事 許可（一般）　第15673号 機械器具設置工事</p>
		</dd>
	</dl>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table4</div>
<div class="c-table4">
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1946年10月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">旋盤・機械加工を主とした合資会社「王子製作所」を東京都中央区木挽町に創立。初代社長に「波多野龍吉」就任。</p>
		</dd>
	</dl>
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1950年6月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">「波多野工業株式会社」に社名変更。</p>
		</dd>
	</dl>
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1952年5月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">営業拡張に伴い東京都港区芝に本社を移転。</p>
		</dd>
	</dl>
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1953年8月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">ヤンマーディーゼル株式会社の特約店となる。</p>
		</dd>
	</dl>
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1959年1月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">資本金を400万円に増資。</p>
		</dd>
	</dl>
	<dl class="c-table4__row">
		<dt class="c-table4__col c-table4__col1">
			<p class="c-table4__ttl">1960年1月</p>
		</dt>
		<dd class="c-table4__col c-table4__col2">
			<p class="c-table4__text">初の500kVA非常用発電機を日本道路公団殿に納入。</p>
		</dd>
	</dl>
</div>