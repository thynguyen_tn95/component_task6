<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1</div>
<div class="c-btn1">
	<a href="" class="c-btn1__text">企画・設計</a>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1 c-btn1--gray</div>
<div class="c-btn1 c-btn1--gray">
	<a href="" class="c-btn1__text">メンテナンス</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn2</div>
<div class="c-btn2">
	<a href="" class="c-btn2__text">前の記事へ</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn3</div>
<div class="c-btn3">
	<a href="" class="c-btn3__text">サイトポリシー</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn4</div>
<div class="c-btn4">
	<a href="" class="c-btn4__text">送信内容の確認</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">l-btn l-btn--center</div>
<div class="l-btn l-btn--center">
	<div class="c-btn4">
		<a href="" class="c-btn4__text">送信内容の確認</a>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btngroup</div>
<div class="c-btngroup">
	<div class="c-btn1">
		<a href="" class="c-btn1__text">企画・設計</a>
	</div>
	<div class="c-btn1">
		<a href="" class="c-btn1__text">施工管理</a>
	</div>
	<div class="c-btn1">
		<a href="" class="c-btn1__text">メンテナンス</a>
	</div>
</div>

<br>
<br>
<br>
<div class="c-btngroup">
	<div class="c-btn2">
		<a href="" class="c-btn2__text">前の記事へ</a>
	</div>
	<div class="c-btn2">
		<a href="" class="c-btn2__text">次の記事へ</a>
	</div>
</div>