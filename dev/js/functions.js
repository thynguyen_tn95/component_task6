//===================================================
// SP menu
//===================================================
$(function() {
	$(".c-menuSP").on('click', function() {
		$(this).toggleClass("is-open");
		$('.c-header__wrap').slideToggle(500);
	});
	$(".is-submenu").on('click', function(e) {
		e.preventDefault();
		if ($(window).width() <= 767) {
			$(this).toggleClass("is-open");
			$(this).next('.c-submenu').slideToggle(500);
		}
	});
});

$(window).on("resize", function () {
	if ($(window).width() > 767) {
		$(".c-header__wrap").removeAttr('style');
		$(".c-submenu").removeAttr('style');
	}
});


//===================================================
// slide of mv
//===================================================
$(".c-mainvisual__slide").slick({
	arrows: false,
	autoplay: true,
	autoplaySpeed: 3000,
	infinite: true,
	fade: true,
	cssEase: "linear"
});

//===================================================
// slide of c-new
//===================================================
$(".c-new__list").slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 3000,
	infinite: true,
	prevArrow: "",
	nextArrow:
	"<img class='slick-next' src='/assets/img/common/icon/icon-arrow-right-black.png'>",
	responsive: [{
		breakpoint: 767,
		settings: {
			adaptiveHeight: true
		}
	}]
});

//===================================================
// matchHeight
//===================================================
$(".js_matchHeight").matchHeight();
